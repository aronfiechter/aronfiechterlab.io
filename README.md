# Home

Welcome to my personal website! I'm Aron Fiechter and I work as a Junior R&D Engineer at [CodeLounge](https://codelounge.si.usi.ch) in Lugano, Switzerland.

I got a MSc in Software and Data Engineering from [USI, Lugano](https://usi.ch) in July 2020.

My interests in the software domain span all things related to developing good quality software, from tools to practices. I like spending a lot of time automating short but repetitive tasks.

Outside of work I like cooking, listening and dancing to music, watching movies and TV series, and playing and collecting Magic: The Gathering (mostly Commander).

See my [projects](/projects), follow me on [Twitter](https://twitter.com/AronFiechter), or read my [CV](/docs/cv.pdf).
