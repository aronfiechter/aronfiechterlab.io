export const siteTitle = 'Aron Fiechter'
export const siteDescription = 'Personal website of Aron Fiechter'
export const siteURL = 'aronfiechter.com'
export const siteLink = 'https://gitlab.com/aronfiechter/aronfiechter.gitlab.io'
export const siteAuthor = 'Aron Fiechter'

// Controls how many posts are shown per page on the main projects index pages
export const postsPerPage = 10

// Edit this to alter the main nav menu. (Also used by the footer and mobile nav.)
export const navItems = [
  {
    title: 'Home',
    route: '/'
  }, {
    title: 'Projects',
    route: '/projects'
  }, {
    title: 'Contact',
    route: '/contact' 
  },
]