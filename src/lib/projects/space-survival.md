---
title: "Space Survival"
date: "2015-12-15"
categories: 
  - "videogame"
  - "functional"
  - "procedural generation"
coverImage: "/images/space-survival.jpg"
coverWidth: 16
coverHeight: 10
excerpt: A procedurally generated space survival game.
---

Space Survival is 2D videogame written in Scheme by Jacopo Fidacaro and me. The player controls a spaceship that and has to find the portal to the next level while avoiding asteroids, sprito cans and Glorbs. There is no end to the game and every level is generated to be incrementally more difficult. After you die three times it's game over.

To play the game on macOS, you can download the [zipped executable](/docs/Space_Survival.app.zip), unzip it, and run it (you might need to right-click it to actually open it, or even allow apps downloaded from anywhere to be run on your machine).

If you prefer (or if you're not on macOS), you can instead download the source code from [GitHub](https://github.com/TiredFalcon/space-survival). To run the game, [download DrRacket](https://download.racket-lang.org/), open the main `Space Survival.rkt` file with it, and hit run.
