---
title: "Discard Humanity"
date: "2016-12-16"
categories: 
  - "videogame"
  - "web app"
  - "polymer"
coverImage: "/images/discard-humanity.png"
coverWidth: 16
coverHeight: 9
excerpt: A Cards Against Humanity clone.
---

Discard Humanity is a Cards Against Humanity clone which features a deck creator. It was implemented as a web application using Polymer 1.x and Liquid.js as part of the course Web Atelier at USI, Lugano.

The currently live version is a reimplementation in React.js done by Renzo Cotti, and is available at [discardhumanity.herokuapp.com](https://discardhumanity.herokuapp.com).
