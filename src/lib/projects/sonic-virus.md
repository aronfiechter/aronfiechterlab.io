---
title: "SonicVirus"
date: "2020-02-07"
categories: 
  - "data visualization"
  - "data sonification"
  - "web app"
  - "vue"
coverImage: "/images/sonic-virus.jpg"
coverWidth: 16
coverHeight: 9
excerpt: A data sonification of the COVID-19 outbreak.
---

<script>
  import YoutubeVideo from '$lib/components/YoutubeVideo.svelte';
</script>

SonicVirus is a data visualization and sonification of the COVID-19 outbreak. It was developed by people as a submission for the WirVsVirus hackathon.

<YoutubeVideo videoId="LB6_xnFrwds" />


SonicVirus is available at [sonicvirus.si.usi.ch](https://sonicvirus.si.usi.ch).
