---
title: "Mímir"
date: "2020-08-18"
categories:
  - "data visualization"
  - "swiss federal archives"
  - "web app"
  - "react"
coverImage: "/images/mimir-sfa.jpg"
coverWidth: 4558
coverHeight: 2933
excerpt: A visual browser for the Swiss Federal Archives
---

<script>
  import YoutubeVideo from '$lib/components/YoutubeVideo.svelte';
</script>

Mímir is a visual platform to explore the Swiss Federal Archives. It aggregates the results according to the archive plan and presents them with Voronoi treemaps.

<YoutubeVideo videoId="wKFd5wOgjSk" />

Mímir is available at [mimir.si.usi.ch](https://mimir.si.usi.ch).
